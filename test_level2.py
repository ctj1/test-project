#!/bin/env python

import solutions1

def test_q2(): # sum
        res = solutions1.subtraction(5,2)
        sol = 3
        assert res == sol, "test2.1 failed"

        res = solutions1.multiplication(5,2)
        sol = 10
        assert res == sol, "test2.2 failed"

